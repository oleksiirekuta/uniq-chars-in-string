import collections
from functools import lru_cache
import argparse


@lru_cache()
def number_of_characters(some_str):
    try:
        counter = collections.Counter(some_str)
        count = sum(1 for c in counter.values() if c == 1)
        return count
    except TypeError:
        print('Wrong type. Please enter a string.')


def process_string(some_str):
    unique_count = number_of_characters(some_str)
    return unique_count


def process_file(file_path):
    try:
        with open(file_path, 'r') as file:
            content = file.read()
            unique_count = number_of_characters(content)
            return unique_count
    except FileNotFoundError:
        print(f"File not found: {file_path}")
        return None


def main():
    parser = argparse.ArgumentParser(description='Count of characters.')
    parser.add_argument('--string', help='Input string.')
    parser.add_argument('--file', help='Enter path to text file.')

    args = parser.parse_args()

    if args.file:
        uniq_char = process_file(args.file)
        if uniq_char is not None:
            print(f"File: {args.file} | Unique Count: {uniq_char}")
    elif args.string:
        uniq_char = process_string(args.string)
        print(f"String: {args.string} | Unique Count: {uniq_char}")
    else:
        print("Please provide either --string or --file parameter")


if __name__ == '__main__':
    main()
