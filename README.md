# Unique characters

### The program will help you calculate the number unique characters in the text you send.

To do this you need to use the command:

Install the package with a command in the terminal:
`pip install -i https://test.pypi.org/simple/ check-your-uniqChars==0.0.2`

If it's a string, use - 
`python -m check_your_uniqChars.uniq_chars --string 'your string'`

Or if it's a file, use -
`python -m check_your_uniqChars.uniq_chars --file path_to_text_file`

You can also pass both arguments, but the file will take precedence. 

`python -m check_your_uniqChars.uniq_chars --string “your string” --file path_to_text_file`


